# Robinson_Crusoe_scenarios

Custom made scenarios for the board game "Robinson Crusoe - Adventures on the Cursed Island (2012)"

## Usage
Just open one of the pdfs and proceed as usual. All special rules are on the corresponding documents, there are no further adjustments of the core rules.

## Support
If you find any rule issues, or balance problems, feel free to contact me.

## Authors and acknowledgment
I am the sole author of this project.
